﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;

public static class Global
{
    public static List<Color> colors;
    public static List<Bitmap> planes;
    public static List<RotateFlipType> rotates;

    static Global()
    {
        colors = new List<Color>() { Color.Green, Color.Blue, Color.Red, Color.Yellow };
        planes = new List<Bitmap>() { new Bitmap("plane_green.png"), new Bitmap("plane_blue.png"), new Bitmap("plane_red.png"), new Bitmap("plane_yellow.png") };
        rotates = new List<RotateFlipType>() { RotateFlipType.Rotate270FlipNone, RotateFlipType.Rotate180FlipNone, RotateFlipType.Rotate90FlipNone, RotateFlipType.RotateNoneFlipNone };
    }

    public static Bitmap get_plane(Color c)
    {
        if (c == Color.Green)
            return planes[0];
        if (c == Color.Blue)
            return planes[1];
        if (c == Color.Red)
            return planes[2];
        if (c == Color.Yellow)
            return planes[3];
        return new Bitmap(1, 1);
    }

    public static RotateFlipType get_rot(dir d) { return rotates[(int) d]; }
}



static class Program
{
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
}
