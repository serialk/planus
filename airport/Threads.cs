﻿using System;
using System.Windows.Forms;
using System.Threading;
using System.ComponentModel;
using System.Drawing;
using System.Collections.Generic;

public enum dir
{
    NORTH,
    WEST,
    SOUTH,
    EST,
}

public class Buffer
{
    private Color planeColor;
    private int hubDestination;
    private bool empty = true;

    public void Read(ref Color planeColor, ref int hubDestination)
    {
        lock (this)
        {
            if (empty)
                Monitor.Wait(this);
            empty = true;
            planeColor = this.planeColor;
            hubDestination = this.hubDestination;
            Monitor.Pulse(this);
        }
    }

    public void Write(Color planeColor, int hubDestination)
    {
        lock (this)
        {
            if (!empty)
                Monitor.Wait(this);
            empty = false;
            this.planeColor = planeColor;
            this.hubDestination = hubDestination;
            Monitor.Pulse(this);
        }
    }

    public void Start()
    {
    }
}

public class Semaphore
{
    private int count = 0;

    public void Wait()
    {
        lock (this)
        {
            while (count == 0)
                Monitor.Wait(this);
            count = 0;
        }
    }

    public void Signal()
    {
        lock (this)
        {
            count = 1;
            Monitor.Pulse(this);
        }
    }

    public void Start()
    {
    }
}

public class ArrivalPanelThread
{
    private Point origin;
    private int delay;
    private Panel panel;
    private dir direction;
    private Color colour;
    private Point plane;
    private int xDelta;
    private int yDelta;
    private Semaphore semaphore;
    private Buffer buffer;
    private Button btn;
    private int destination = 0;
    private List<RadioButton> destinations_boxes;
    private Bitmap b;


    public ArrivalPanelThread(Point origin,
                              int delay,
                              dir direction,
                              Panel panel,
                              Semaphore semaphore,
                              Buffer buffer,
                              Button btn,
                              List<RadioButton> destinations_boxes)
    {
        this.origin = origin;
        this.delay = delay;
        this.direction = direction;
        this.panel = panel;
        this.destinations_boxes = destinations_boxes;

        this.plane = origin;
        this.panel.Paint += new PaintEventHandler(this.panel_Paint);
        this.xDelta = ((int)direction & 1) * ((int)direction - 2) * 10;
        this.yDelta = (1 - ((int)direction & 1)) * ((int)direction - 1) * 10;
        this.semaphore = semaphore;
        this.buffer = buffer;
        this.btn = btn;
        this.btn.Click += new System.EventHandler(this.btn_Click);
        b = new Bitmap(Global.get_plane(Color.White));
    }

    private void btn_Click(object sender,
                           System.EventArgs e)
    {
        lock (this)
        {
            Monitor.Pulse(this);
        }
    }

    public void Start()
    {
        Thread.Sleep(delay);
        while (true)
        {
            int d = delay;
            this.zeroPlane();
            panel.Invalidate();
            lock (this)
            {
                Monitor.Wait(this);
            }
            setBtnEnabled(false);
            btn.Invalidate();
            for (int i = 0; i < destinations_boxes.Count; i++)
            {
                if (destinations_boxes[i].Checked)
                {
                    destination = i;
                    colour = Global.colors[i];
                    b = new Bitmap(Global.get_plane(colour));
                    b.RotateFlip(Global.get_rot(direction));
                    panel.Invalidate();
                }
            }
            while (plane.X + 2 * xDelta >= 0 && plane.Y + 2 * yDelta >= 0 && plane.X + 2 * xDelta < panel.Size.Width && plane.Y + 2 * yDelta < panel.Size.Height)
            {
                this.movePlane(xDelta, yDelta);
                Thread.Sleep(d);
                panel.Invalidate();
                d += 3;
            }
            semaphore.Wait();
            buffer.Write(this.colour, destination);
            setBtnEnabled(true);
            btn.Invalidate();
        }
    }

    private void zeroPlane()
    {
        plane.X = origin.X;
        plane.Y = origin.Y;
    }

    private void movePlane(int xDelta, int yDelta)
    {
        plane.X += xDelta; plane.Y += yDelta;
    }

    private void panel_Paint(object sender, PaintEventArgs e)
    {
        Graphics g = e.Graphics;
        g.DrawImage((Image)b, plane.X, plane.Y, 20, 20);

        /*SolidBrush brush = new SolidBrush(colour);
        g.FillRectangle(brush, plane.X, plane.Y, 10, 10);

        brush.Dispose();*/
        g.Dispose();
    }

    delegate void setBtnEnabledCallback(bool b);
    public void setBtnEnabled(bool b)
    {
        if (this.btn.InvokeRequired)
        {
            setBtnEnabledCallback d = new setBtnEnabledCallback(setBtnEnabled);
            btn.Invoke(d, new object[] { b });
        }
        else
        {
            this.btn.Enabled = b;
        }
    }
}

public class ButtonPanelThread
{
    private Point origin;
    private int delay;
    private Panel panel;
    private dir direction;
    private Color colour;
    private Point plane;
    private int xDelta;
    private int yDelta;
    private Semaphore semaphore;
    private Buffer buffer;
    private Semaphore semaphore_next;
    private Buffer buffer_next;
    private Button btn;
    private Bitmap b;

    public ButtonPanelThread(Point origin,
                             int delay,
                             dir direction,
                             Panel panel,
                             Color colour,
                             Semaphore semaphore,
                             Buffer buffer,
                             Semaphore semaphore_next,
                             Buffer buffer_next,
                             Button btn)
    {
        this.origin = origin;
        this.delay = delay;
        this.direction = direction;
        this.panel = panel;
        this.colour = colour;

        this.plane = origin;
        this.panel.Paint += new PaintEventHandler(this.panel_Paint);
        this.xDelta = ((int)direction & 1) * ((int)direction - 2) * 10;
        this.yDelta = (1 - ((int)direction & 1)) * ((int)direction - 1) * 10;
        this.semaphore_next = semaphore_next;
        this.buffer_next = buffer_next;
        this.semaphore = semaphore;
        this.buffer = buffer;
        this.btn = btn;
        this.btn.Click += new System.EventHandler(this.btn_Click);
    }

    private void btn_Click(object sender,
                           System.EventArgs e)
    {
        lock (this)
        {
            Monitor.Pulse(this);
        }
    }

    public void Start()
    {
        Thread.Sleep(delay);
        int trash = 0;
        setBtnEnabled(false);
        while (true)
        {
            this.colour = Color.White;
            b = new Bitmap(Global.get_plane(colour));
            panel.Invalidate();

            semaphore.Signal();
            this.zeroPlane();
            buffer.Read(ref this.colour, ref trash);
            b = new Bitmap(Global.get_plane(colour));
            b.RotateFlip(Global.get_rot(direction));
            b.RotateFlip(RotateFlipType.Rotate180FlipNone);

            while (plane.Y + 2 * (- yDelta) >= 30)
            {
                panel.Invalidate();
                this.movePlane(-xDelta, -yDelta);
                Thread.Sleep(delay);
            }
            b.RotateFlip(RotateFlipType.Rotate180FlipNone);
            panel.Invalidate();

            setBtnEnabled(true);
            lock (this)
            {
                Monitor.Wait(this);
            }
            setBtnEnabled(false);
            while (plane.X + 2 * xDelta >= 0 && plane.Y + 2 * yDelta >= 0 && plane.X + 2 * xDelta < panel.Size.Width && plane.Y + 2 * yDelta < panel.Size.Height)
            {
                this.movePlane(xDelta, yDelta);
                Thread.Sleep(delay);
                panel.Invalidate();
            }
            semaphore_next.Wait();
            buffer_next.Write(this.colour, 0);
        }
    }

    private void zeroPlane()
    {
        plane.X = origin.X;
        plane.Y = origin.Y;
    }

    private void movePlane(int xDelta, int yDelta)
    {
        plane.X += xDelta; plane.Y += yDelta;
    }

    private void panel_Paint(object sender, PaintEventArgs e)
    {
        Graphics g = e.Graphics;
        g.DrawImage((Image) b, plane.X, plane.Y, 20, 20);

        /*SolidBrush brush = new SolidBrush(colour);
        g.FillRectangle(brush, plane.X, plane.Y, 10, 10);
        brush.Dispose();*/

        g.Dispose();
    }


    delegate void setBtnEnabledCallback(bool b);
    public void setBtnEnabled(bool b)
    {
        if (this.btn.InvokeRequired)
        {
            setBtnEnabledCallback d = new setBtnEnabledCallback(setBtnEnabled);
            btn.Invoke(d, new object[] { b });
        }
        else
        {
            this.btn.Enabled = b;
        }
    }
}

public class WaitPanelThread
{
    private Point origin;
    private int delay;
    private Panel panel;
    private Color colour;
    private Point plane;
    private int xDelta;
    private int yDelta;
    private Semaphore semaphore, s_arrival, s_arrival_s;
    private Buffer buffer, b_arrival, b_arrival_s;
    private dir direction;
    private int destination, i_arrival_s;
    private Bitmap b;
    int sa = 0;

    public WaitPanelThread(Point origin,
                       int delay,
                       dir direction,
                       Panel panel,
                       Color colour,
                       Semaphore semaphore,
                       Buffer buffer,
                       Semaphore s_arrival = null,
                       Buffer b_arrival = null,
                       Semaphore s_arrival_s = null,
                       Buffer b_arrival_s = null,
                       int i_arrival_s = -1)
    {
        this.origin = origin;
        this.delay = delay;
        this.direction = direction;
        this.panel = panel;
        this.colour = colour;
        this.plane = origin;
        this.panel.Paint += new PaintEventHandler(this.panel_Paint);
        this.xDelta = ((int)direction & 1) * ((int)direction - 2) * 10;
        this.yDelta = (1 - ((int)direction & 1)) * ((int)direction - 1) * 10;
        this.semaphore = semaphore;
        this.buffer = buffer;
        this.s_arrival = s_arrival;
        this.b_arrival = b_arrival;
        this.s_arrival_s = s_arrival_s;
        this.b_arrival_s = b_arrival_s;
        this.i_arrival_s = i_arrival_s;
    }

    public void Start()
    {
        this.colour = Color.White;
        b = new Bitmap(Global.get_plane(colour));
        b.RotateFlip(Global.get_rot(direction));
        while (true)
        {
            int delay = this.delay;
            sa = 0;
            semaphore.Signal();
            this.zeroPlane();

            buffer.Read(ref this.colour, ref this.destination);
            b = new Bitmap(Global.get_plane(colour));
            b.RotateFlip(Global.get_rot(direction));

            while (plane.X + 2.5 * xDelta >= 0 && plane.Y + 2.5 * yDelta >= 0 && plane.X + 2.5 * xDelta < panel.Size.Width && plane.Y + 2.5 * yDelta < panel.Size.Height)
            {
                panel.Invalidate();
                this.movePlane(xDelta, yDelta);
                Thread.Sleep(delay);
                if (s_arrival == null)
                {
                    if (delay >= 10)
                        delay--;
                    else if (sa < 50)
                        sa++;
                }
            }
            if (s_arrival_s != null && b_arrival_s != null && i_arrival_s == destination)
            {
                s_arrival_s.Wait();
                b_arrival_s.Write(colour, destination);
            }
            else if (s_arrival != null && b_arrival != null)
            {
                s_arrival.Wait();
                b_arrival.Write(colour, destination);
            }
            this.colour = Color.White;
            b = new Bitmap(Global.get_plane(colour));
            b.RotateFlip(Global.get_rot(direction));
            panel.Invalidate();
        }
    }

    private void zeroPlane()
    {
        plane.X = origin.X;
        plane.Y = origin.Y;
    }

    private void movePlane(int xDelta, int yDelta)
    {
        plane.X += xDelta; plane.Y += yDelta;
    }

    private void panel_Paint(object sender, PaintEventArgs e)
    {
        Graphics g = e.Graphics;
        lock (this)
        {
            g.DrawImage((Image)b, plane.X - sa / 2, plane.Y - sa / 2, 20 + sa, 20 + sa);
        }
        /*
        SolidBrush brush = new SolidBrush(colour);
        g.FillRectangle(brush, plane.X, plane.Y, 10, 10);
        brush.Dispose();*/
        g.Dispose();
    }
}