﻿using System;
using System.Windows.Forms;
using System.Threading;
using System.ComponentModel;
using System.Drawing;
using System.Collections.Generic;

public class Form1 : Form
{
    private Container components = null;

    private ButtonPanelThread h1, h2, h3;
    private ArrivalPanelThread ha;

    private WaitPanelThread w1, w2, w3, w4, w5, w6, w7, wtakeoff;
    private Semaphore semaphore_wait1, semaphore_wait2, semaphore_wait3, semaphore_wait4, semaphore_wait5, semaphore_wait6, semaphore_wait7, semaphore_takeoff, semaphore_hub1, semaphore_hub2, semaphore_hub3;
    private Buffer buffer_wait1, buffer_wait2, buffer_wait3, buffer_wait4, buffer_wait5, buffer_wait6, buffer_wait7, buffer_takeoff, buffer_hub1, buffer_hub2, buffer_hub3;
    private Thread sem_thread_wait1, sem_thread_wait2, sem_thread_wait3, sem_thread_wait4, sem_thread_wait5, sem_thread_wait6, sem_thread_wait7, sem_thread_takeoff, sem_thread_hub1, sem_thread_hub2, sem_thread_hub3;
    private Thread buff_thread_wait1, buff_thread_wait2, buff_thread_wait3, buff_thread_wait4, buff_thread_wait5, buff_thread_wait6, buff_thread_wait7, buff_thread_takeoff, buff_thread_hub1, buff_thread_hub2, buff_thread_hub3;
    private Thread thread_w1, thread_w2, thread_w3, thread_w4, thread_w5, thread_w6, thread_w7, thread_takeoff;
    private Thread thread_h1, thread_h2, thread_h3, thread_ha;
    private Button btnhub1;
    private Panel hub1, pnl2;
    private Panel section1;
    private Panel hub2;
    private Button btnhub2;
    private Panel section4;
    private Panel section5;
    private Panel hub3;
    private Button btnhub3;
    private Panel arrival;
    private Button btnarrival;
    private Panel panel3;
    private Panel section3;
    private Panel panel4;
    private Panel section2;
    private GroupBox groupBox1;
    private RadioButton radioButton2;
    private RadioButton radioButton1;
    private RadioButton radioButton4;
    private RadioButton radioButton3;
    private Panel takeoff;
    private Panel section7;
    private Panel section6;
    private Panel panel1;
    private Panel panel2;
    private Panel panel5;
    private Panel panel6;
    private Label label1;
    private Label label2;
    private List<RadioButton> destinations_box;

    public Form1()
    {
        InitializeComponent();

        destinations_box = new List<RadioButton>() {radioButton1, radioButton2, radioButton3, radioButton4};

        semaphore_wait1 = new Semaphore();
        buffer_wait1 = new Buffer();
        semaphore_wait2 = new Semaphore();
        buffer_wait2 = new Buffer();
        semaphore_wait3 = new Semaphore();
        buffer_wait3 = new Buffer();
        semaphore_wait4 = new Semaphore();
        buffer_wait4 = new Buffer();
        semaphore_wait5 = new Semaphore();
        buffer_wait5 = new Buffer();
        semaphore_wait6 = new Semaphore();
        buffer_wait6 = new Buffer();
        semaphore_wait7 = new Semaphore();
        buffer_wait7 = new Buffer();
        semaphore_takeoff = new Semaphore();
        buffer_takeoff = new Buffer();
        semaphore_hub1 = new Semaphore();
        buffer_hub1 = new Buffer();
        semaphore_hub2 = new Semaphore();
        buffer_hub2 = new Buffer();
        semaphore_hub3 = new Semaphore();
        buffer_hub3 = new Buffer();


        h1 = new ButtonPanelThread(new Point(5, 170),
                    40, dir.SOUTH, hub1,
                    Global.colors[1],
                    semaphore_hub1,
                    buffer_hub1,
                    semaphore_wait1,
                    buffer_wait1,
                    btnhub1);

        h2 = new ButtonPanelThread(new Point(5, 170),
                    40, dir.SOUTH, hub2,
                    Global.colors[2],
                    semaphore_hub2,
                    buffer_hub2,
                    semaphore_wait2,
                    buffer_wait2,
                    btnhub2);

        h3 = new ButtonPanelThread(new Point(5, 170),
                    40, dir.SOUTH, hub3,
                    Global.colors[3],
                    semaphore_hub3,
                    buffer_hub3,
                    semaphore_wait3,
                    buffer_wait3,
                    btnhub3);

        ha = new ArrivalPanelThread(new Point(260, 5),
             5, dir.WEST, arrival,
             semaphore_wait5,
             buffer_wait5,
             btnarrival,
             destinations_box);

        w1 = new WaitPanelThread(new Point(5, 5),
                             40, dir.EST, section1,
                             Color.White,
                             semaphore_wait1,
                             buffer_wait1,
                             semaphore_wait2,
                             buffer_wait2,
                             semaphore_hub2,
                             buffer_hub2,
                             2);

        w2 = new WaitPanelThread(new Point(5, 5),
                             40, dir.EST, section2,
                             Color.White,
                             semaphore_wait2,
                             buffer_wait2,
                             semaphore_wait3,
                             buffer_wait3,
                             semaphore_hub3,
                             buffer_hub3,
                             3);

        w3 = new WaitPanelThread(new Point(5, 5),
                             40, dir.EST, section3,
                             Color.White,
                             semaphore_wait3,
                             buffer_wait3,
                             semaphore_wait4,
                             buffer_wait4);

        w4 = new WaitPanelThread(new Point(5, 5),
                     40, dir.SOUTH, section4,
                     Color.White,
                     semaphore_wait4,
                     buffer_wait4,
                     semaphore_wait5,
                     buffer_wait5);

        w5 = new WaitPanelThread(new Point(680, 5),
                     40, dir.WEST, section5,
                     Color.White,
                     semaphore_wait5,
                     buffer_wait5,
                     semaphore_wait6,
                     buffer_wait6,
                     semaphore_wait7,
                     buffer_wait7,
                     0);

        w6 = new WaitPanelThread(new Point(5, 200),
                     40, dir.NORTH, section6,
                     Color.White,
                     semaphore_wait6,
                     buffer_wait6,
                     semaphore_wait1,
                     buffer_wait1,
                     semaphore_hub1,
                     buffer_hub1,
                     1);

        w7 = new WaitPanelThread(new Point(5, 5),
                     40, dir.SOUTH, section7,
                     Color.White,
                     semaphore_wait7,
                     buffer_wait7,
                     semaphore_takeoff,
                     buffer_takeoff);

        wtakeoff = new WaitPanelThread(new Point(5, 35),
             40, dir.EST, takeoff,
             Color.White,
             semaphore_takeoff,
             buffer_takeoff);


        sem_thread_wait1 = new Thread(new ThreadStart(semaphore_wait1.Start));
        sem_thread_wait2 = new Thread(new ThreadStart(semaphore_wait2.Start));
        sem_thread_wait3 = new Thread(new ThreadStart(semaphore_wait3.Start));
        sem_thread_wait4 = new Thread(new ThreadStart(semaphore_wait4.Start));
        sem_thread_wait5 = new Thread(new ThreadStart(semaphore_wait5.Start));
        sem_thread_wait6 = new Thread(new ThreadStart(semaphore_wait6.Start));
        sem_thread_wait7 = new Thread(new ThreadStart(semaphore_wait7.Start));
        sem_thread_takeoff = new Thread(new ThreadStart(semaphore_takeoff.Start));
        sem_thread_hub1 = new Thread(new ThreadStart(semaphore_hub1.Start));
        sem_thread_hub2 = new Thread(new ThreadStart(semaphore_hub2.Start));
        sem_thread_hub3 = new Thread(new ThreadStart(semaphore_hub3.Start));
        buff_thread_wait1 = new Thread(new ThreadStart(buffer_wait1.Start));
        buff_thread_wait2 = new Thread(new ThreadStart(buffer_wait2.Start));
        buff_thread_wait3 = new Thread(new ThreadStart(buffer_wait3.Start));
        buff_thread_wait4 = new Thread(new ThreadStart(buffer_wait4.Start));
        buff_thread_wait5 = new Thread(new ThreadStart(buffer_wait5.Start));
        buff_thread_wait6 = new Thread(new ThreadStart(buffer_wait6.Start));
        buff_thread_wait7 = new Thread(new ThreadStart(buffer_wait6.Start));
        buff_thread_takeoff = new Thread(new ThreadStart(buffer_takeoff.Start));
        buff_thread_hub1 = new Thread(new ThreadStart(buffer_hub1.Start));
        buff_thread_hub2 = new Thread(new ThreadStart(buffer_hub2.Start));
        buff_thread_hub3 = new Thread(new ThreadStart(buffer_hub3.Start));
        thread_w1 = new Thread(new ThreadStart(w1.Start));
        thread_w2 = new Thread(new ThreadStart(w2.Start));
        thread_w3 = new Thread(new ThreadStart(w3.Start));
        thread_w4 = new Thread(new ThreadStart(w4.Start));
        thread_w5 = new Thread(new ThreadStart(w5.Start));
        thread_w6 = new Thread(new ThreadStart(w6.Start));
        thread_w7 = new Thread(new ThreadStart(w7.Start));
        thread_takeoff = new Thread(new ThreadStart(wtakeoff.Start));
        thread_h1 = new Thread(new ThreadStart(h1.Start));
        thread_h2 = new Thread(new ThreadStart(h2.Start));
        thread_h3 = new Thread(new ThreadStart(h3.Start));
        thread_ha = new Thread(new ThreadStart(ha.Start));

        thread_h1.Start();
        thread_h2.Start();
        thread_h3.Start();
        thread_ha.Start();

        sem_thread_wait1.Start();
        buff_thread_wait1.Start();
        thread_w1.Start();
        
        sem_thread_wait2.Start();
        buff_thread_wait2.Start();
        thread_w2.Start();
        
        sem_thread_wait3.Start();
        buff_thread_wait3.Start();
        thread_w3.Start();

        sem_thread_wait4.Start();
        buff_thread_wait4.Start();
        thread_w4.Start();

        sem_thread_wait5.Start();
        buff_thread_wait5.Start();
        thread_w5.Start();

        sem_thread_wait6.Start();
        buff_thread_wait6.Start();
        thread_w6.Start();

        sem_thread_wait7.Start();
        buff_thread_wait7.Start();
        thread_w7.Start();

        sem_thread_takeoff.Start();
        buff_thread_takeoff.Start();
        thread_takeoff.Start();

        sem_thread_hub1.Start();
        sem_thread_hub2.Start();
        sem_thread_hub3.Start();
        buff_thread_hub1.Start();
        buff_thread_hub2.Start();
        buff_thread_hub3.Start();

        this.Closing += new CancelEventHandler(this.Form1_Closing);
    }


    protected override void Dispose(bool disposing)
    {
        if (disposing)
        {
            if (components != null)
                components.Dispose();
        }
        base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
        this.hub1 = new System.Windows.Forms.Panel();
        this.btnhub1 = new System.Windows.Forms.Button();
        this.pnl2 = new System.Windows.Forms.Panel();
        this.section1 = new System.Windows.Forms.Panel();
        this.hub2 = new System.Windows.Forms.Panel();
        this.panel4 = new System.Windows.Forms.Panel();
        this.btnhub2 = new System.Windows.Forms.Button();
        this.panel3 = new System.Windows.Forms.Panel();
        this.section4 = new System.Windows.Forms.Panel();
        this.section5 = new System.Windows.Forms.Panel();
        this.section6 = new System.Windows.Forms.Panel();
        this.hub3 = new System.Windows.Forms.Panel();
        this.btnhub3 = new System.Windows.Forms.Button();
        this.arrival = new System.Windows.Forms.Panel();
        this.btnarrival = new System.Windows.Forms.Button();
        this.section3 = new System.Windows.Forms.Panel();
        this.section2 = new System.Windows.Forms.Panel();
        this.groupBox1 = new System.Windows.Forms.GroupBox();
        this.radioButton4 = new System.Windows.Forms.RadioButton();
        this.radioButton3 = new System.Windows.Forms.RadioButton();
        this.radioButton2 = new System.Windows.Forms.RadioButton();
        this.radioButton1 = new System.Windows.Forms.RadioButton();
        this.takeoff = new System.Windows.Forms.Panel();
        this.section7 = new System.Windows.Forms.Panel();
        this.panel1 = new System.Windows.Forms.Panel();
        this.panel2 = new System.Windows.Forms.Panel();
        this.panel5 = new System.Windows.Forms.Panel();
        this.panel6 = new System.Windows.Forms.Panel();
        this.label1 = new System.Windows.Forms.Label();
        this.label2 = new System.Windows.Forms.Label();
        this.hub1.SuspendLayout();
        this.hub2.SuspendLayout();
        this.hub3.SuspendLayout();
        this.groupBox1.SuspendLayout();
        this.SuspendLayout();
        // 
        // hub1
        // 
        this.hub1.BackColor = System.Drawing.Color.White;
        this.hub1.Controls.Add(this.btnhub1);
        this.hub1.Controls.Add(this.pnl2);
        this.hub1.Location = new System.Drawing.Point(39, 12);
        this.hub1.Name = "hub1";
        this.hub1.Size = new System.Drawing.Size(30, 178);
        this.hub1.TabIndex = 0;
        // 
        // btnhub1
        // 
        this.btnhub1.Location = new System.Drawing.Point(0, 0);
        this.btnhub1.Name = "btnhub1";
        this.btnhub1.Size = new System.Drawing.Size(30, 30);
        this.btnhub1.TabIndex = 0;
        this.btnhub1.Text = "1";
        this.btnhub1.UseVisualStyleBackColor = true;
        // 
        // pnl2
        // 
        this.pnl2.BackColor = System.Drawing.Color.White;
        this.pnl2.Location = new System.Drawing.Point(0, 181);
        this.pnl2.Name = "pnl2";
        this.pnl2.Size = new System.Drawing.Size(260, 30);
        this.pnl2.TabIndex = 1;
        // 
        // section1
        // 
        this.section1.BackColor = System.Drawing.Color.White;
        this.section1.Location = new System.Drawing.Point(39, 189);
        this.section1.Name = "section1";
        this.section1.Size = new System.Drawing.Size(222, 31);
        this.section1.TabIndex = 2;
        // 
        // hub2
        // 
        this.hub2.BackColor = System.Drawing.Color.White;
        this.hub2.Controls.Add(this.panel4);
        this.hub2.Controls.Add(this.btnhub2);
        this.hub2.Controls.Add(this.panel3);
        this.hub2.Location = new System.Drawing.Point(257, 12);
        this.hub2.Name = "hub2";
        this.hub2.Size = new System.Drawing.Size(30, 178);
        this.hub2.TabIndex = 1;
        // 
        // panel4
        // 
        this.panel4.BackColor = System.Drawing.Color.White;
        this.panel4.Location = new System.Drawing.Point(10, 177);
        this.panel4.Name = "panel4";
        this.panel4.Size = new System.Drawing.Size(212, 31);
        this.panel4.TabIndex = 5;
        // 
        // btnhub2
        // 
        this.btnhub2.Location = new System.Drawing.Point(0, 0);
        this.btnhub2.Name = "btnhub2";
        this.btnhub2.Size = new System.Drawing.Size(30, 30);
        this.btnhub2.TabIndex = 0;
        this.btnhub2.Text = "2";
        this.btnhub2.UseVisualStyleBackColor = true;
        // 
        // panel3
        // 
        this.panel3.BackColor = System.Drawing.Color.White;
        this.panel3.Location = new System.Drawing.Point(3, 177);
        this.panel3.Name = "panel3";
        this.panel3.Size = new System.Drawing.Size(212, 31);
        this.panel3.TabIndex = 3;
        // 
        // section4
        // 
        this.section4.BackColor = System.Drawing.Color.White;
        this.section4.Location = new System.Drawing.Point(693, 189);
        this.section4.Name = "section4";
        this.section4.Size = new System.Drawing.Size(35, 248);
        this.section4.TabIndex = 4;
        // 
        // section5
        // 
        this.section5.BackColor = System.Drawing.Color.White;
        this.section5.Location = new System.Drawing.Point(39, 432);
        this.section5.Name = "section5";
        this.section5.Size = new System.Drawing.Size(689, 31);
        this.section5.TabIndex = 4;
        // 
        // section6
        // 
        this.section6.BackColor = System.Drawing.Color.White;
        this.section6.Location = new System.Drawing.Point(39, 216);
        this.section6.Name = "section6";
        this.section6.Size = new System.Drawing.Size(35, 221);
        this.section6.TabIndex = 5;
        // 
        // hub3
        // 
        this.hub3.BackColor = System.Drawing.Color.White;
        this.hub3.Controls.Add(this.btnhub3);
        this.hub3.Location = new System.Drawing.Point(475, 12);
        this.hub3.Name = "hub3";
        this.hub3.Size = new System.Drawing.Size(30, 178);
        this.hub3.TabIndex = 2;
        // 
        // btnhub3
        // 
        this.btnhub3.Location = new System.Drawing.Point(0, 0);
        this.btnhub3.Name = "btnhub3";
        this.btnhub3.Size = new System.Drawing.Size(30, 30);
        this.btnhub3.TabIndex = 0;
        this.btnhub3.Text = "3";
        this.btnhub3.UseVisualStyleBackColor = true;
        // 
        // arrival
        // 
        this.arrival.BackColor = System.Drawing.Color.White;
        this.arrival.Location = new System.Drawing.Point(727, 433);
        this.arrival.Name = "arrival";
        this.arrival.Size = new System.Drawing.Size(258, 30);
        this.arrival.TabIndex = 3;
        // 
        // btnarrival
        // 
        this.btnarrival.Location = new System.Drawing.Point(772, 215);
        this.btnarrival.Name = "btnarrival";
        this.btnarrival.Size = new System.Drawing.Size(194, 30);
        this.btnarrival.TabIndex = 0;
        this.btnarrival.Text = "Arrival";
        this.btnarrival.UseVisualStyleBackColor = true;
        // 
        // section3
        // 
        this.section3.BackColor = System.Drawing.Color.White;
        this.section3.Location = new System.Drawing.Point(475, 189);
        this.section3.Name = "section3";
        this.section3.Size = new System.Drawing.Size(221, 31);
        this.section3.TabIndex = 4;
        // 
        // section2
        // 
        this.section2.BackColor = System.Drawing.Color.White;
        this.section2.Location = new System.Drawing.Point(257, 189);
        this.section2.Name = "section2";
        this.section2.Size = new System.Drawing.Size(222, 31);
        this.section2.TabIndex = 5;
        // 
        // groupBox1
        // 
        this.groupBox1.Controls.Add(this.radioButton4);
        this.groupBox1.Controls.Add(this.radioButton3);
        this.groupBox1.Controls.Add(this.radioButton2);
        this.groupBox1.Controls.Add(this.radioButton1);
        this.groupBox1.Location = new System.Drawing.Point(772, 251);
        this.groupBox1.Name = "groupBox1";
        this.groupBox1.Size = new System.Drawing.Size(194, 143);
        this.groupBox1.TabIndex = 6;
        this.groupBox1.TabStop = false;
        this.groupBox1.Text = "Arrival destination";
        // 
        // radioButton4
        // 
        this.radioButton4.AutoSize = true;
        this.radioButton4.Location = new System.Drawing.Point(42, 104);
        this.radioButton4.Name = "radioButton4";
        this.radioButton4.Size = new System.Drawing.Size(54, 17);
        this.radioButton4.TabIndex = 3;
        this.radioButton4.TabStop = true;
        this.radioButton4.Text = "Hub 3";
        this.radioButton4.UseVisualStyleBackColor = true;
        // 
        // radioButton3
        // 
        this.radioButton3.AutoSize = true;
        this.radioButton3.Location = new System.Drawing.Point(42, 81);
        this.radioButton3.Name = "radioButton3";
        this.radioButton3.Size = new System.Drawing.Size(54, 17);
        this.radioButton3.TabIndex = 2;
        this.radioButton3.TabStop = true;
        this.radioButton3.Text = "Hub 2";
        this.radioButton3.UseVisualStyleBackColor = true;
        // 
        // radioButton2
        // 
        this.radioButton2.AutoSize = true;
        this.radioButton2.Location = new System.Drawing.Point(42, 58);
        this.radioButton2.Name = "radioButton2";
        this.radioButton2.Size = new System.Drawing.Size(54, 17);
        this.radioButton2.TabIndex = 1;
        this.radioButton2.TabStop = true;
        this.radioButton2.Text = "Hub 1";
        this.radioButton2.UseVisualStyleBackColor = true;
        // 
        // radioButton1
        // 
        this.radioButton1.AutoSize = true;
        this.radioButton1.Checked = true;
        this.radioButton1.Location = new System.Drawing.Point(42, 35);
        this.radioButton1.Name = "radioButton1";
        this.radioButton1.Size = new System.Drawing.Size(109, 17);
        this.radioButton1.TabIndex = 0;
        this.radioButton1.TabStop = true;
        this.radioButton1.Text = "Immediate takeoff";
        this.radioButton1.UseVisualStyleBackColor = true;
        // 
        // takeoff
        // 
        this.takeoff.BackColor = System.Drawing.Color.White;
        this.takeoff.Location = new System.Drawing.Point(72, 499);
        this.takeoff.Name = "takeoff";
        this.takeoff.Size = new System.Drawing.Size(913, 87);
        this.takeoff.TabIndex = 5;
        // 
        // section7
        // 
        this.section7.BackColor = System.Drawing.Color.White;
        this.section7.Location = new System.Drawing.Point(39, 459);
        this.section7.Name = "section7";
        this.section7.Size = new System.Drawing.Size(35, 101);
        this.section7.TabIndex = 6;
        // 
        // panel1
        // 
        this.panel1.BackColor = System.Drawing.Color.LemonChiffon;
        this.panel1.Location = new System.Drawing.Point(447, 0);
        this.panel1.Name = "panel1";
        this.panel1.Size = new System.Drawing.Size(87, 149);
        this.panel1.TabIndex = 7;
        // 
        // panel2
        // 
        this.panel2.BackColor = System.Drawing.Color.Tomato;
        this.panel2.Location = new System.Drawing.Point(230, 0);
        this.panel2.Name = "panel2";
        this.panel2.Size = new System.Drawing.Size(87, 149);
        this.panel2.TabIndex = 8;
        // 
        // panel5
        // 
        this.panel5.BackColor = System.Drawing.Color.DeepSkyBlue;
        this.panel5.Location = new System.Drawing.Point(12, 0);
        this.panel5.Name = "panel5";
        this.panel5.Size = new System.Drawing.Size(87, 149);
        this.panel5.TabIndex = 8;
        // 
        // panel6
        // 
        this.panel6.BackColor = System.Drawing.Color.White;
        this.panel6.Location = new System.Drawing.Point(39, 559);
        this.panel6.Name = "panel6";
        this.panel6.Size = new System.Drawing.Size(35, 27);
        this.panel6.TabIndex = 7;
        // 
        // label1
        // 
        this.label1.AutoSize = true;
        this.label1.Font = new System.Drawing.Font("Malgun Gothic", 30.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label1.Location = new System.Drawing.Point(634, 35);
        this.label1.Name = "label1";
        this.label1.Size = new System.Drawing.Size(289, 55);
        this.label1.TabIndex = 9;
        this.label1.Text = "Planus Airport";
        // 
        // label2
        // 
        this.label2.AutoSize = true;
        this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label2.Location = new System.Drawing.Point(648, 103);
        this.label2.Name = "label2";
        this.label2.Size = new System.Drawing.Size(275, 17);
        this.label2.TabIndex = 10;
        this.label2.Text = "A surrealistic airport simulator in real time !";
        // 
        // Form1
        // 
        this.BackColor = System.Drawing.Color.LightGray;
        this.ClientSize = new System.Drawing.Size(978, 616);
        this.Controls.Add(this.label2);
        this.Controls.Add(this.label1);
        this.Controls.Add(this.panel6);
        this.Controls.Add(this.takeoff);
        this.Controls.Add(this.section7);
        this.Controls.Add(this.btnarrival);
        this.Controls.Add(this.groupBox1);
        this.Controls.Add(this.section2);
        this.Controls.Add(this.section3);
        this.Controls.Add(this.arrival);
        this.Controls.Add(this.hub3);
        this.Controls.Add(this.section6);
        this.Controls.Add(this.section5);
        this.Controls.Add(this.section4);
        this.Controls.Add(this.hub2);
        this.Controls.Add(this.section1);
        this.Controls.Add(this.hub1);
        this.Controls.Add(this.panel1);
        this.Controls.Add(this.panel5);
        this.Controls.Add(this.panel2);
        this.Name = "Form1";
        this.Text = "Planus Airport";
        this.Closing += new System.ComponentModel.CancelEventHandler(this.Form1_Closing);
        this.Load += new System.EventHandler(this.Form1_Load);
        this.hub1.ResumeLayout(false);
        this.hub2.ResumeLayout(false);
        this.hub3.ResumeLayout(false);
        this.groupBox1.ResumeLayout(false);
        this.groupBox1.PerformLayout();
        this.ResumeLayout(false);
        this.PerformLayout();

    }

    private void Form1_Closing(object sender, CancelEventArgs e)
    {
        Environment.Exit(Environment.ExitCode);
    }

    private void Form1_Load(object sender, EventArgs e)
    {

    }
}


